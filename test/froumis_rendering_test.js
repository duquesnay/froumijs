casper.setUp = function () {
    casper.evaluate(function () {
        froumi.moveToPosition({left: 0, top: 0});
    });
};

casper.setUpPointy = function () {
    casper.evaluate(function () {
        froumi.graphical_avatar.width(0);
        froumi.graphical_avatar.height(0);
        froumi.moveToPosition({left: 0, top: 0});
    });
}

casper.test.begin('Froumi should has an image avatar', function suite(test) {
    casper.start('./froumis.html');

    casper.then(function () {
        test.assertExists('#froumi_img', "there is a froumi avatar");
        test.assertExists('img[id="froumi_img"]', "froumi avatar is an image")
    });

    casper.run(function () {
        test.done()
    });
});

casper.test.begin("image position is identical when width/height are 0", function suite(test) {
    casper.start('./froumis.html', casper.setUpPointy);

    casper.then(function () {
        assertAvatarLeftOffset(test, 0, 'image position identical horizontally');
        assertAvatarTopOffset(test, 0, 'image position identical vertically');
    });

    casper.run(function () {
        test.done();
    });

});

casper.test.begin("image is centered on the position (image position is deported to the top left corner)", function suite(test) {
    casper.start('./froumis.html', casper.setUp);
    // GIVEN
    casper.thenEvaluate(
        function () {
            $(froumi_img).width('100');
            $(froumi_img).height('100');
        }
    );

    // WHEN
    casper.thenEvaluate(function () {
        froumi.moveToPosition({left: 0, top: 0});
    });

    // THEN
    casper.then(function () {
        assertAvatarLeftOffset(test, -50, "image centered horizontally");
        assertAvatarTopOffset(test, -50, "image centered vertically");
    });

    casper.run(function () {
        test.done();
    });

});

// absolute position

casper.test.begin("Froumi can be moved at any position", function suite(test) {
    casper.start('./froumi.html', casper.setUpPointy);

    casper.thenEvaluate(function () {
        froumi.moveToPosition({left: 53, top: 47});
    });
    casper.then(function () {
        assertAvatarLeftOffset(test, 53, "Froumi positionned itself correctly on horizontal axis");
        assertAvatarTopOffset(test, 47, "Froumi positionned itself correctly on vertical axis");
    });
    casper.run(function () {
        test.done()
    });
});

casper.test.begin('Froumi image should have jquery rotate loaded°', function suite(test) {
    casper.start('./froumis.html', function () {
        assertAvatarRotateAngleEquals(test, 0);
    });

    casper.run(function () {
        test.done()
    });
});

casper.test.begin('Froumi image should rotate when fourmi turn right°', function suite(test) {
    casper.start('./froumis.html', casper.setUpPointy);

    casper.thenEvaluate(function () {
        froumi.turnRight(45);
    });

    casper.then(function () {
        assertAvatarRotateAngleEquals(test, 45);
    })

    casper.run(function () {
        test.done()
    });
});

casper.test.begin('Froumi image should rotate again when fourmi turn again (relative angle)°', function suite(test) {
    casper.start('./froumis.html');

    casper.thenEvaluate(function () {
        froumi.turnRight(45);
    });
    casper.thenEvaluate(function () {
        froumi.turnRight(45);
    });


    casper.then(function () {
        assertAvatarRotateAngleEquals(test, 90);
    })

    casper.run(function () {
        test.done()
    });
});

function assertHorizontalPosition(test, expected, message) {
    test.assertEvalEquals(
        function () {
            return froumi.position.left
        },
        expected,
        message
    );
}
function assertVerticalPosition(test, expected, message) {
    test.assertEvalEquals(
        function () {
            return froumi.position.top
        },
        expected,
        message
    );
}
casper.test.begin("position manipulation and moves are still ok after rotation", function suite(test) {
    casper.start('./froumis.html', casper.setUp);
    casper.thenEvaluate(function () {
        $(froumi_img).width('100');
        $(froumi_img).height('100');
        froumi.turnRight(120);
    });

    casper.thenEvaluate(function () {
        froumi.moveToPosition({left: 53, top: 74});
    });

    casper.then(function () {
        assertHorizontalPosition(test, 53, "Froumi horizontal position ok after moveToPosition");
        assertVerticalPosition(test, 74, "Froumi vertical position ok after moveToPosition");
    });

    casper.thenEvaluate(function () {
        froumi.moveForward();
    });

    casper.then(function () {
        assertHorizontalPosition(test, 53 + 9, "Froumi horizontal position incremented by one step on 60°");
        assertVerticalPosition(test, 74 + 5, "Froumi vertical position incremented by one step on 60°");
    });
    casper.run(function () {
        test.done();
    });
});

function print_offset(offset) {
    console.log("offset is left:" + offset.left + ",top:" + offset.top);
}

function print_froumi_offset() {
    print_offset(
        casper.evaluate(function () {
            return $(froumi_img).offset();

        })
    );
}


casper.test.begin("Froumi should get it's avatar parameters at init", function (test) {
    test.done()
});

function assertAvatarLeftOffset(test, lef_pos, message) {
    test.assertEvalEquals(function () {
        $(froumi_img).rotate(0);
        return $(froumi_img).offset().left;
    }, lef_pos, message);
}

function assertAvatarTopOffset(test, top_pos, message) {
    test.assertEvalEquals(function () {
        $(froumi_img).rotate(0);
        return $(froumi_img).position().top;
    }, top_pos, message);
}

function assertAvatarRotateAngleEquals(test, expectedAngle) {
    test.assertEvalEquals(function () {
        return $(froumi_img).getRotateAngle()[0]
    }, expectedAngle, "image rotation is " + expectedAngle);
}

