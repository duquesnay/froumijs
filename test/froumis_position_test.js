casper.setUp = function () {
    casper.evaluate(function () {
        $(froumi_img).width(0);
        $(froumi_img).height(0);
        froumi.moveToPosition({left:0,top:0});
        froumi.setOrientation(0);
    });
};

casper.test.begin('Froumi should have an avatar', function suite(test) {
    casper.start('./froumis.html');

    casper.then(function () {
        test.assertExists('#froumi_img');
    });

    casper.run(function () {
        test.done()
    });
});

// Basic move

casper.test.begin("Froumi can move one step", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, 0, "no horizontal move");
        assertTopOffset(test, -10, "Froumi moved by 10 pixels");
    });
    casper.run(function () {
        test.done()
    });
});

casper.test.begin("Froumi can move many steps", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.thenEvaluate(function () {
        froumi.moveForward(3);
    });
    casper.then(function () {
        assertTopOffset(test, -30, "Froumi moved by 30 pixels");
    });
    casper.run(function () {
        test.done()
    });
});

// Turning

casper.test.begin("Froumi starts turning north (angle 0)", function suite(test) {
    casper.start('./froumi.html',casper.setUp);

    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, 0, "Froumi didn't move horizontally");
        test.assertEvaluate(function () {
            return $(froumi_img).offset().top < 0;
        }, "Froumi moved upward");
    });
    casper.run(function () {
        test.done()
    });

})

casper.test.begin("Froumi should turn right 90° (to horizontal right)", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.then(function () {
        assertLeftOffset(test, 0, "sanity check : at 0");
        assertTopOffset(test, 0, "sanity check : at 0");
    });
    casper.thenEvaluate(function () {
        froumi.turnRight(90);
    });
    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, 10, "Shift right 10 pixels");
        assertTopOffset(test, 0, "No vertical shift");
    });
    casper.run(function () {
        test.done()
    });
});

casper.test.begin("Froumi should turn right at any angle (like 60°)", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.thenEvaluate(function () {
        froumi.turnRight(60);
    });
    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, 9, "Shift to the right by 8 pixels");
        assertTopOffset(test, -5, "Shift top by 5 pixels");
    });
    casper.run(function () {
        test.done()
    });
});

casper.test.begin("Froumi should turn left at any angle (like 60°)", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.thenEvaluate(function () {
        froumi.turnLeft(60);
    });
    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, -9, "Shift to the right by 5 pixels");
        assertTopOffset(test, -5, "Shift up by 8 pixels");
    });
    casper.run(function () {
        test.done()
    });
});

casper.test.begin("Froumi can even turn back (just in case, like 150°)", function suite(test) {
    casper.start('./froumi.html', casper.setUp);

    casper.thenEvaluate(function () {
        froumi.turnLeft(150);
    });
    casper.thenEvaluate(function () {
        froumi.moveForward();
    });
    casper.then(function () {
        assertLeftOffset(test, -5, "Shift to the left by 5 pixels");
        assertTopOffset(test, 9, "Shift up by 8 pixels");
    });
    casper.run(function () {
        test.done()
    });
});


function print_offset(offset) {
    console.log("offset is left:" + offset.left + ",top:" + offset.top);
}
function print_froumi_offset() {
    print_offset(casper.evaluate(function () {
        return $(froumi_img).position();

    }));
}
function assertLeftOffset(test, lef_pos, message) {
    test.assertEvalEquals(function () {
        $(froumi_img).rotate(0);//normalize for offset checking
        return $(froumi_img).offset().left;
    }, lef_pos, message);
}
function assertTopOffset(test, top_pos, message) {
    test.assertEvalEquals(function () {
        $(froumi_img).rotate(0);//normalize for offset checking
        return $(froumi_img).offset().top;
    }, top_pos, message);
}
