module.exports = function (grunt) {
    grunt.initConfig({
        qunit: {
            all: ['qunit-tests/**/*.html']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.registerTask('default', ['qunit']);
}