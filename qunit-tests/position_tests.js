module('Position properties', {
    setup:function setup(){
        this.a_position = new Position({left: 10, top: 11});
    }
})

test("Position can be instantiated", function () {
    ok( this.a_position )
});
test("Position has left and top properties", function () {
    ok(this.a_position.left, "left exists");
    ok(this.a_position.top, "top exists");
})

module('Position.add()', {
    setup:function setup() {
        this.a_position = new Position({left: 10, top: 11});
        this.a_float_position = new Position({left: 10.3, top: 10.7});
    }
})
test('should return a Position', function () {
    var shift = {left: 12, top: 23};
    var r = this.a_position.add(shift);
    ok(r instanceof Position);
})
test('can be added', function () {
    var shift = {left: 12, top: 23};
    var r = this.a_position.add(shift);
    deepEqual({left: r.left, top: r.top }, {left: 22, top: 34 }, "left/top matches addition");
})

module('Position.multiply()', {
    setup:function setup() {
        this.a_position = new Position({left: 10, top: 11});
        this.a_float_position = new Position({left: 10.3, top: 10.7});
    }

})

test('Position multiplication is a Position', function () {
    var factor = 2;
    var r = this.a_position.multiply(2);
    ok(r instanceof Position);
})
test('can be mutliplied by 0', function () {
    var factor = 0;
    var r = this.a_position.multiply(factor);
    deepEqual({left: r.left, top: r.top }, {left: 0, top: 0 }, "left/top matches 0");
})
test('can be mutliplied by 1', function () {
    var factor = 1;
    var r = this.a_position.multiply(factor);
    deepEqual({left: r.left, top: r.top }, {left: 10, top: 11 }, "left/top matches same value");
})
test('can be mutliplied by -1', function () {
    var factor = -1;
    var r = this.a_position.multiply(factor);
    deepEqual({left: r.left, top: r.top }, {left: -10, top: -11 }, "left/top are reversed");
})
test('can be mutliplied by anything, even a float', function () {
    var factor = 2.4;
    var r = this.a_position.multiply(factor);
    deepEqual({left: r.left, top: r.top }, {left: 24, top: 26.4 }, "left/top matches float mutiplication");
})

module('Position.round()', {
    setup:function setup() {
        this.a_float_position = new Position({left: 10.3, top: 10.7});
    }
})
test('Position rounded is a Position', function () {
    var r = this.a_float_position.round()
    ok(r instanceof Position);
})
test('can be rounded lower or higher', function () {
    var r = this.a_float_position.round();
    deepEqual({left: r.left, top: r.top }, {left: 10, top: 11 }, "left/top rounded");
})

module('Position.shift', {

    setup:function setup() {
        this.a_position= new Position({left: 10, top: 11});
    }
})
test('can be shifted north', function () {
    var r = this.a_position.shift(0,10);
    deepEqual({left: r.left, top: r.top }, {left: 10, top: 1 }, "up by 10");
})
test('can be shifted west', function () {
    var r = this.a_position.shift(90,10);
    deepEqual({left: r.left, top: r.top }, {left: 20, top: 11 }, "rigght by 10");
})
test('can be shifted south', function () {
    var r = this.a_position.shift(180,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 10, top: 21 }, "down by 10");
})
test('can be shifted east', function () {
    var r = this.a_position.shift(270,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 0, top: 11 }, "left by 10");
})
test('can be shifted north west any angle', function () {
    var r = this.a_position.shift(-60,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 1, top: 6 }, "left by 10");
})
test('can be shifted north east', function () {
    var r = this.a_position.shift(60,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 19, top: 6 }, "left by 10");
})
test('can be shifted south east by any angle', function () {
    var r = this.a_position.shift(150,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 15, top: 20 }, "left by 10");
})
test('can be shifted north east by any angle', function () {
    var r = this.a_position.shift(-120,10).round();
    deepEqual({left: r.left, top: r.top }, {left: 1, top: 16 }, "left by 10");
})

//TODO turn it into x and y and toOffset and fromOffset
