var Position = function Position(offset) {
    this.left = offset.left;
    this.top = offset.top;

    this.shift = function (angle, distance) {
        return this
            .add(
                angleToOffset(angle)
                    .multiply(distance)
            );
    };
    this.add = function (shift) {
        return new Position({
            left: this.left + shift.left,
            top: this.top + shift.top
        });
    }
    this.multiply = function (factor) {
        return new Position({
            left: this.left * factor,
            top: this.top * factor
        });
    }
    this.round = function () {
        return new Position({
            left: Math.round(this.left),
            top: Math.round(this.top)
        });
    }

    this.toString = function () {
        return "{" + left + "," + top + "}";
    }

    //private
    function angleToOffset(angle) {
        return new Position({
            left: horizontalAngleProjection(angle),
            top: verticalAngleProjection(angle)
        });
    }

    function horizontalAngleProjection(angle) {
        return Math.sin(angle / 360 * Math.PI * 2);
    }

    function verticalAngleProjection(angle) {
        return -Math.cos(angle / 360 * Math.PI * 2);
    }

}
