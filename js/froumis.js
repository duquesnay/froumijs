var Froumi = function Froumi(avatar) {
    const pixelPerStep = 10;
    this.position = new Position({left: 0, top: 0});
    this.graphical_avatar = avatar;

    this.moveForward = function (steps) {
        steps = steps || 1
        var newPosition =
            this.position
                .shift(
                    this.orientation,
                    steps * pixelPerStep
                ).round();
        this.moveToPosition(newPosition);
    }
    this.setOrientation = function (angle) {
        this.orientation = angle;
        this.graphical_avatar.rotate(this.orientation);
    }
    this.setOrientation(0);

    this.turnRight = function (angle) {
        this.setOrientation(this.orientation + angle);
    }
    this.turnLeft = function (angle) {
        this.turnRight(-angle);
    }
    this.moveToPosition = function (position) {
        this.position = new Position(position);
        var newPosition = avatarRelativeOffset(this.graphical_avatar).add(this.position).round();
        setOffset(
            this.graphical_avatar,
            newPosition);
    }

    //private
    function getOffset(graphicalavatar) {
        return new Position(
            graphicalavatar.offset()
        );
    }

    function setOffset(graphicalavatar, newPosition) {
        graphicalavatar.offset(
            newPosition
        );
    }

    function avatarRelativeOffset(graphicalavatar) {
        return new Position({
            left: graphicalavatar.width(),
            top: graphicalavatar.height()
        }).multiply(-1 / 2);
    }

}

this.Froumi = Froumi;